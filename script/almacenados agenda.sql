use agendain6av;



create table usuario(
	idUsuario int Auto_increment,
    nick varchar(60),
    contrasena varchar(60),
    imagen varchar(200),
    PRIMARY KEY(idUsuario)
);

create table contacto(
	idContacto int AUTO_INCREMENT,
    nombre varchar(30),
    apellido varchar(30),
    direccion varchar(30),
    telefono varchar(30),
    correo varchar(30),
	imagen varchar (200),
	idCategoria int,
    PRIMARY KEY(idContacto),
    FOREIGN KEY(idCategoria) references Categoria(idCategoria)
);

CREATE TABLE detalleContacto(
	idDetalleUsuario int AUTO_INCREMENT,
    idContacto int,
    idUsuario int,
    PRIMARY KEY(idDetalleUsuario),
    FOREIGN KEY(idContacto) references contacto(idContacto),
    FOREIGN KEY(idUsuario) references usuario(idUsuario)
);
CREATE TABLE detalleCategoria(
	idDetalleUsuario int AUTO_INCREMENT,
    idCategoria int,
    idUsuario int,
    PRIMARY KEY(idDetalleUsuario),
    FOREIGN KEY(idCategoria) references categoria(idCategoria),
    FOREIGN KEY(idUsuario) references usuario(idUsuario)
);

CREATE TABLE cita(
	idCita INT AUTO_INCREMENT,
    fecha datetime,
    lugar varchar(50),
    idContacto int,
    PRIMARY KEY(idCita),
    FOREIGN KEY(idContacto) REFERENCES contacto(idContacto)
);

create table prioridad(
	idPrioridad INT AUTO_INCREMENT,
    nombrePrioridad varchar(60),
    PRIMARY KEY(idPrioridad)
);

CREATE TABLE tarea(
	idTarea INT AUTO_INCREMENT,
    nombre varchar(60),
    descripcion varchar (60),
    idPrioridad int, 
	fecha dateTime,
    categoria varchar(30),
    PRIMARY KEY(idTarea),
    FOREIGN KEY(idPrioridad) REFERENCES prioridad(idPrioridad)
);
create table detalleTarea(
	idDetalleTarea int Auto_increment,
    idUsuario int,
    idTarea int,
    primary key(idDetalleTarea),
    FOREIGN KEY(idUsuario) references  usuario(idUsuario),
    FOREIGN KEY(idTarea) references tarea(idTarea)
);
create table detalleCita(
	idDetalleCita int Auto_increment,
    idUsuario int,
    idCita int,
    primary key(idDetalleCita),
    FOREIGN KEY(idUsuario) references  usuario(idUsuario),
    FOREIGN KEY(idCita) references cita(idCita)
);
drop procedure 

DELIMITER &&

CREATE PROCEDURE sp_InsertarContacto (IN _idUsuario INT, IN _idCategoria INT , IN _nombre VARCHAR(20), 
	IN _apellido VARCHAR(20), IN _direccion VARCHAR(20), IN _telefono VARCHAR(20), 
    IN _correo VARCHAR(20), IN _imagen VARCHAR(200))
	BEGIN
    DECLARE _idContacto INT;
    
    INSERT INTO Contacto(idCategoria, nombre, apellido, 
		direccion, telefono, correo, imagen)         
        VALUES(_idCategoria, _nombre, _apellido, _direccion, _telefono, _correo, _imagen);
    
    SET _idContacto = (SELECT MAX(idContacto) FROM Contacto);
    
    INSERT INTO DetalleContacto(idUsuario, idContacto) 
		VALUES(_idUsuario, _idContacto);
    END&&
    
DELIMITER ; 



DELIMITER &&
CREATE PROCEDURE sp_ModificarContacto (IN _idCategoria INT , IN _nombre VARCHAR(20), 
	IN _apellido VARCHAR(20), IN _direccion VARCHAR(20), IN _telefono VARCHAR(20), 
    IN _correo VARCHAR(20),  in _idContacto int)
	BEGIN
    
    update contacto set idCategoria = _idCategoria, nombre = _nombre, apellido = _apellido,
						direccion = _direccion, telefono = _telefono, correo = _correo, imagen = _imagen where idContacto = _idContacto;
    
    END&&
    
DELIMITER ; 
drop procedure sp_ModificarContacto;
DELIMITER &&

CREATE PROCEDURE sp_EliminarContacto (IN _idContacto int)
	BEGIN
    DELETE FROM detalleContacto where idContacto = _idContacto;
    DELETE FROM contacto where idContacto = _idContacto;
    END&&
    
DELIMITER ; 
call sp_EliminarContacto(1);

-----------------------------------------------------------

DELIMITER &&

CREATE PROCEDURE sp_InsertarCategoria (IN _idUsuario INT, IN _nombre VARCHAR(20))
	BEGIN
	DECLARE  _idCategoria int;

    INSERT INTO categoria(nombreCategoria)         
        VALUES(_nombre);
    
    SET _idCategoria = (SELECT MAX(idCategoria) FROM Categoria);
    
    INSERT INTO DetalleCategoria(idUsuario, idCategoria) 
		VALUES(_idUsuario, _idCategoria);
    END&&
    
DELIMITER ; 



DELIMITER &&

create PROCEDURE sp_ModificarCategoria ( IN _nombre VARCHAR(20), in _idcategoria int)
	BEGIN
	
    
    update categoria set nombreCategoria = _nombre where idCategoria = _idcategoria;
    
    END&&
    
   
DELIMITER ; 

DELIMITER &&

CREATE PROCEDURE sp_EliminarCategoria (IN _idCategoria int)
	BEGIN
    DELETE FROM detalleCategoria where idCategoria = _idCategoria;
    DELETE FROM categoria where idCategoria = _idCategoria;
    END&&
    
DELIMITER ; 


-----------------------------------------
DELIMITER &&

CREATE PROCEDURE sp_InsertarCita (IN _idContacto INT, IN _idUsuario INT, IN _fecha datetime,  IN _lugar varchar (60))
	BEGIN
    declare _idCita int;
    INSERT INTO cita(idContacto, fecha, lugar) values(_idContacto, _fecha, _lugar);
     SET _idCita = (SELECT MAX(idCita) FROM Cita);
    
    INSERT INTO DetalleCita(idUsuario, idCita) 
		VALUES(_idUsuario, _idCita);
    END&&
    
DELIMITER ; 

DELIMITER &&

CREATE PROCEDURE sp_ModificarCita (IN _idCita int, IN _idContacto INT, IN _fecha datetime,  IN _lugar varchar (60))
	BEGIN
    
	update cita set idContacto = _idContacto, fecha = _fecha, lugar = _lugar where idCita = _idCita;
   END&&
    
DELIMITER ; 

DELIMITER &&

CREATE PROCEDURE sp_EliminarCita (IN _idCita int)
	BEGIN
    
	delete from detalleCita where idCita = _idCita;
    DELETE FROM cita WHERE idCita = _idCita;
    END&&
    
DELIMITER ; 



DELIMITER &&

CREATE PROCEDURE sp_insertarTarea (IN _nombre varchar(60), IN _descripcion varchar(60), in _fecha varchar(60), in _categoria varchar(60), in _idPrioridad int, in _idUsuario int)
	BEGIN
    declare _idContacto int;
    insert into tarea(nombre, descripcion, fecha, categoria, idPrioridad) 
				values(_nombre, _descripcion, _fecha, _categoria, _idPrioridad);
      SET _idContacto = (SELECT MAX(idTarea) FROM Tarea);
    
    INSERT INTO DetalleTarea(idUsuario, idTarea) 
		VALUES(_idUsuario, _idTarea);
   
   END&&
    
DELIMITER ; 


DELIMITER &&

CREATE PROCEDURE sp_ModificarTarea (IN _nombre varchar(60), IN _descripcion varchar(60), in _fecha varchar(60), in _categoria varchar(60), in _idPrioridad int, IN _idTarea int)
	BEGIN
    update tarea set nombre = _nombre, descripcion = _descripcion, fecha = _fecha, categoria = _categoria, idPrioridad = _idPrioridad where idTarea = _idTarea;
   END&&
    
DELIMITER ; 

DELIMITER &&

CREATE PROCEDURE sp_EliminarTarea (IN _idTarea int)
	BEGIN
    delete from detalleTarea where idTarea = _idTarea;
    delete from tarea where idTarea = _idTarea;
    
   END&&
    
DELIMITER ; 


create table Historial(
	idHistorial int Auto_increment,
    Descripcion varchar(60),
    Fecha datetime,
    idUsuario int ,
    primary key(idHistorial),
	FOREIGN KEY (idUsuario) REFERENCES usuario(idUsuario) 
);
  
Select * from Historial;

CREATE VIEW
 contacto_usuario
	AS
    SELECT c. idContacto, c.nombre, c.apellido, c.direccion, c.telefono, c.correo, category.nombreCategoria, u.idUsuario
    FROM Contacto AS c
    INNER JOIN Categoria AS category ON c.idCategoria = category.idCategoria
    INNER JOIN DetalleContacto AS det ON det.idContacto = c.idContacto
    INNER JOIN Usuario As u ON det.idUsuario = u.idUsuario
 
 drop view categoria_usuario
 CREATE VIEW 
	categoria_usuario
    AS
    SELECT c.idCategoria, c.nombreCategoria, u.idUsuario
    from categoria AS c
    INNER JOIN DetalleCategoria AS det ON det.idCategoria = c.idCategoria
   INNER JOIN Usuario As u ON det.idUsuario = u.idUsuario
  
  CREATE VIEW 
	cita_usuario
    AS
    SELECT c.idCita, c.fecha, c.lugar, u.idUsuario,  con.idContacto, con.nombre
    from cita AS c
    INNER JOIN DetalleCita AS det ON det.idCita = c.idCita
	INNER JOIN Usuario As u ON det.idUsuario = u.idUsuario
	INNER JOIN contacto as con on con.idContacto = c.idContacto;












DELIMITER &&
CREATE PROCEDURE sp_selectContacto (in _idU int )
	BEGIN
    
    SELECT c.idContacto, c.nombre,c.direccion, c.apellido, c.telefono, c.correo, c.imagen, c.idCategoria
		from contacto as c inner join detallecontacto as det on c.idContacto = det.idContacto 
        where det.idUsuario = _idU; 
    
    END&&
    
DELIMITER ; 


DELIMITER &&
CREATE PROCEDURE sp_ModificarContacto (in _idContacto int,  IN _nombre VARCHAR(20), 
	IN _apellido VARCHAR(20), IN _direccion VARCHAR(20), IN _telefono VARCHAR(20), 
    IN _correo VARCHAR(20), IN _idCategoria INT )
	BEGIN
    
    update contacto set idCategoria = _idCategoria, nombre = _nombre, apellido = _apellido,
						direccion = _direccion, telefono = _telefono, correo = _correo, imagen = _imagen where idContacto = _idContacto;
    
    END&&
    
DELIMITER ; 



DELIMITER &&
CREATE PROCEDURE sp_ModificarContacto (IN _idCategoria INT , IN _nombre VARCHAR(20), 
	IN _apellido VARCHAR(20), IN _direccion VARCHAR(20), IN _telefono VARCHAR(20), 
    IN _correo VARCHAR(20),  in _idContacto int)
	BEGIN
    
    update contacto set idCategoria = _idCategoria, nombre = _nombre, apellido = _apellido,
						direccion = _direccion, telefono = _telefono, correo = _correo where idContacto = _idContacto;
    
    END&&
    
DELIMITER ; 
drop procedure sp_ModificarContacto;


DELIMITER &&

CREATE PROCEDURE sp_InsertarContacto (IN _idUsuario INT, IN _idCategoria INT , IN _nombre VARCHAR(20), 
	IN _apellido VARCHAR(20), IN _direccion VARCHAR(20), IN _telefono VARCHAR(20), 
    IN _correo VARCHAR(20))
	BEGIN
    DECLARE _idContacto INT;
    
    INSERT INTO Contacto(idCategoria, nombre, apellido, 
		direccion, telefono, correo)         
        VALUES(_idCategoria, _nombre, _apellido, _direccion, _telefono, _correo);
    
    SET _idContacto = (SELECT MAX(idContacto) FROM Contacto);
    
    INSERT INTO DetalleContacto(idUsuario, idContacto) 
		VALUES(_idUsuario, _idContacto);
    END&&
    
DELIMITER ; 








show procedure status;
drop procedure sp_selectContacto
Select * from contacto;



	
    
    select * from cita;
	select * from contacto;
    select * from usuario;
    select * from detalleCategoria;
    select * from categoria;
    select * from contacto_usuario where idUsuario = 1;
	select * from cita_usuario where idUsuario = 1 ;
call sp_InsertarCategoria(1, 'prueba');
call sp_ModificarCategoria('hola2', 7);
call sp_EliminarCategoria(7);
call sp_ModificarCita(1, 2, now(), 'su casa');
call sp_EliminarCita(2);
