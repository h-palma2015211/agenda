var database = require("../config/database.config");
var Contacto = {};

Contacto.selectAll = function( callback) {
  if(database) {
		database.query('SELECT * FROM contacto',
     function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados[0]);
			}
		});
	}
}

Contacto.select = function(idUsuario , callback) {
  if(database) {
		database.query('CALL sp_selectContacto(?)', idUsuario,
     function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados[0]);
			}
		});
	}
}
Contacto.insert = function(data, callback) {
  if(database) {
    database.query('CALL sp_InsertarContacto(?,?,?,?,?,?,?)',
    [data.idUsuario, data.idCategoria, data.nombre, data.apellido, data.telefono, data.direccion, data.correo ],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}

Contacto.update = function(data, callback){
	if(database) {
		database.query('CALL sp_ModificarContacto(?,?,?,?,?,?,?)',
		[data.idCategoria, data.nombre, data.apellido, data.direccion,  data.telefono, data.correo,  data.idContacto],
		function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(resultado[0]);
			}
		});
	}
}

Contacto.delete = function(idContacto, callback) {
	if(database) {
		database.query('CALL sp_EliminarContacto(?)', idContacto,
		function(error, resultado){
			if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
		});
	}
}

Contacto.bus = function(idContacto, callback){
	if(database){
		database.query('Select * from contacto where idContacto =?', idContacto, function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(resultado[0]);
			}
		});
	}
}

module.exports = Contacto;
