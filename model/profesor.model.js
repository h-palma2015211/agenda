var database = require("../config/database.config");
var profesor = {};

profesor.selectAll = function(callback) {
    if(database) {
        var consulta = 'SELECT * FROM profesor ORDER BY idProfesor DESC';
        database.query(consulta, function(error, resultado){
            if(error) throw error;
            callback(resultado);
        });
    }
}
profesor.mostrarAsi = function(callback) {
    if(database) {
        database.query('call sp_selectasignacion()', function( error, resultado){
            if(error) throw error;
            callback(resultado);
        })
    }
}
profesor.asignar = function(data, callback) {
    if(database) {
        database.query('CALL sp_insertarAsignacion( ?, ?, ?)',
        [data.idMateria, data.idProfesor, data.idSeccion],
        function(error, resultado){
            if(error) throw error;
            callback({"affectedRows": resultado.affectedRows});
        })
    }
}
profesor.SelectProfesor = function(idProfesor, callback){
    if(database) {
        database.query('SELECT * FROM profesor where idProfesor = ?',
         idProfesor, function(error, resultado){
            if(error) throw error;
            callback(resultado);
         });
    }
}

profesor.insert = function(data, callback) {
    if(database) {
        database.query('Call sp_insertarProfesor(?, ?, ?, ?, ?, ?)',
        [data.nombre, data.apellido, data.direccion, data.telefono, data.nick, data.contrasena],
        function(error, resultado){
            if(error) throw error;
            callback({"affectedRows": resultado.affectedRows});
        });
    }
}
profesor.update = function(data, callback){
    if(database) {
        database.query('Call sp_updateProfesor( ?, ?, ?, ?, ?)', 
        [data.nombre, data.apellido, data.direccion, data.telefonoo, data.idProfesor],
        function(error, resultado){
            if(error) throw error;
            callback(resultado);
        });
    }
}
profesor.delete = function(idProfesor, callback) {
    if(database) {
        database.query('Call sp_deleteProfesor(?)', idProfesor,
        function(error, resultado){
            if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
        });
    }
}
module.exports = profesor;