var database = require("../config/database.config");
var Tarea = {};

Tarea.select = function(idUsuario, callback) {
    if(database) {
        database.query('CALL sp_selectTarea(?)',idUsuario, function(error, resultados){
            if(error){
                throw error;
            } else{
                callback(resultados[0]);
            }
        });
    }
}

Tarea.insert = function(data, callback) {
    if(database) {
        
        database.query('call sp_insertarTarea(?, ?, ?, ?, ?, ?)', [data.nombre, 
                            data.descripcion, 
                            data.fecha,
                            data.categoria,
                            data.idPrioridad, 
                            data.idUsuario], function(error, resultados){
                               if(error) {
			                	throw error;
		            	        } else {
				                callback(resultados[0]);
		                	    } 
                            });
    }
}


Tarea.update = function(data, callback) {
    if(database){
        var sql = 'CALL sp_ModificarTarea(?, ?, ?, ?, ?, ?)';
        database.query(sql, [data.nombre, 
                            data.descripcion, 
                            data.fecha,
                            data.categoria,
                            data.idPrioridad, 
                            data.idTarea], function(error, resultados){
                                        if(error) {
			                	            throw error;
		            	                } else {
				                          callback(resultados[0]);
		                	            } 
                            });
    }
}

Tarea.delete = function(idTarea, callback) {
    if(database) {
        console.log("idTarea" +idTarea);
        database.query('Call sp_EliminarTarea(?)', idTarea, function(error, resultado){
            if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}

        });
    }

}

module.exports = Tarea;