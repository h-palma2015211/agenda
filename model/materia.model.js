var database = require("../config/database.config");
var materia = {};

materia.select = function(callback) {
    if(database) {
        var consulta = 'CALL sp_Materia()';
        database.query(consulta, function( error, resultado){
             if(error) throw error;
            callback(resultado[0]);
        })
    }
}

materia.selectMateria = function(idMateria, callback) {
    if(database){
        database.query('SELECT * FROM materia where idMateria = ? ', idMateria,
    function(error, resultado){
        if(error) throw error;
        callback(resultado);
    })
    }
}

materia.insert = function(data, callback){
    if(database) {
        database.query('CALL spinsertarMateria(?,?)', [data.nombre, data.token],
    function(error, resultado){
          if(error) throw error;
            callback({"affectedRows": resultado.affectedRows});
    });
    }
}

materia.asignada = function(idUsuario, callback) {
    if(database){
        database.query('CALL sp_seccionAlumnoMateria(?)', idUsuario,
    function(error, resultado){
        if(error) throw error;
        callback(resultado[0]);
    })
    }
}
materia.update = function(data, callback){
    if(database) {
        database.query('CALL spModificarMateria(?, ?)', [data.nombre, data.idMateria],
    function(error, resultado){
        if(error) throw error;
            callback(resultado);
    })
    }
}

materia.delete = function(idMateria, callback) {
    if(database) {
        database.query(' CALL spEliminarMateria(?)', idMateria,
    function(error, resultado) {
        if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
    })
    }   
}
module.exports = materia;