var database = require("../config/database.config");
var Historial = {};

Historial.select = function(callback) {
    if(database) {
        database.query('SELECT * FROM historial', function(error, resultados){
            if(error) {
				throw error;
			} else {
				callback(resultados);
			}
        });
    }
}

module.exports = Historial;