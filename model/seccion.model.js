var database = require("../config/database.config");
var Seccion = {};

Seccion.select = function(callback) {
    if(database) {
        database.query('Select * from seccion', function(error, resultados){
            if(error) {
				throw error;
			} else {
				callback(resultados[0]);
			}
        })
    }
}
module.exports = Seccion;