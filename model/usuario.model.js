var database = require("../config/database.config");
var usuario = {};

var randToken = require('rand-token').suid;

usuario.token = function(callback) {
	var token = randToken(16);
	var token1 = randToken(16);
	var token2 = randToken(16);
	var token3 = randToken(16);
	var token4 = randToken(16);
	var token5 = randToken(16);
	var token6 = randToken(16);
	var token7 = randToken(16);
	var token8 = randToken(16);
	var token9 = randToken(16);
	var token10 = randToken(16);
	var token11 = randToken(16);
	var token12 = randToken(16);
	var token13 = randToken(16);
	var token14 = randToken(16);
	var token15 = randToken(16);

	var s ={
		token1: token,
		token2: token1,
		token3: token2,
		token4: token3,
		token5: token4,
		token6: token5,
		token7: token6,
		token8: token7,
		token9: token8,
		token10: token9,
		token11: token10,
		token12: token11,
		token13: token12,
		token14: token13,
		token15: token14


	};
	if(database) {
		database.query('call sp_AToken(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
		 [s.token1, s.token2, s.token3, s.token4, s.token5, s.token6, s.token7,
			s.token8, s.token9, s.token10, s.token11, s.token12, s.token13, s.token14, s.token15],
		function(error, resultados){
			if(error){
				throw error;
			} else {
				callback(s);
			}
		})

	}

}


usuario.login = function(data, callback) {
  if(database) {
    var consulta = 'CALL sp_Autenticar(?, ?);';
		database.query(consulta, [data.nick, data.contrasena], function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(resultado[0]);
				console.log(resultado[0]);
			}
		});
	}
}


usuario.selectAll = function(callback) {
	if(database) {
		var consulta = 'SELECT * FROM Usuario';
		database.query(consulta, function(error, resultado){
			if(error) throw error;
			callback(resultado);
		});
	}
}
usuario.selectEs = function(idUsuario, callback) {
	if(database) {
	
		database.query('SELECT * FROM Usuario where idUsuario = ?', idUsuario, function(error, resultado){
			if(error) throw error;
			callback(resultado);
		});
	}
}

usuario.selectUsuario = function(idUsuario, callback) {
  if(database) {
    var consulta = 'CALL sp_selectUsuario(?);';
		database.query(consulta, idUsuario, function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(resultado[0]);
			}
		});
	}
}

usuario.selectHistorial = function(idUsuario, callback) {
  if(database) {
    var consulta = 'CALL sp_selectHistorial(?);';
		database.query(consulta, idUsuario, function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(resultado[0]);
			}
		});
  }
}


usuario.insert = function(data, callback) {
  if(database) {
    database.query('INSERT INTO usuario (nick, contrasena) VALUES(?,?)',
    [data.nick, data.contrasena],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}

usuario.update = function(data, callback){
	if(database) {
		database.query('CALL sp_updateUsuario(?,?,?)',
		[data.idUsuario, data.nick, data.contrasena],
		function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(data);
			}
		});
	}
}

usuario.delete = function(idUsuario, callback) {
	if(database) {
		database.query('DELETE FROM usuario where idUsuario = ?', idUsuario,
		function(error, resultado){
			if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
		});
	}
}

module.exports = usuario;
