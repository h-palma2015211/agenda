
var express = require('express');
var jwt = require('jsonwebtoken');
var usuario = require('../model/usuario.model');
var router = express.Router();
var to = require('../model/token.model');
var pdf = require('pdfkit');
var fs = require('fs');


router.post('/auth/', function(req, res) {
	var data = {
		nick: req.body.nick,
		contrasena: req.body.contrasena
	}
	usuario.login(data, function(resultado) {
		if(typeof resultado !== undefined) {

			var token = 'Bearer ' + jwt.sign(resultado[0], 'in6av', { expiresIn: 60 * 60 });
			resultado[0].estado = true;
			resultado[0].mensaje = "Se otorgo el acceso";
			resultado[0].token = token;
			resultado[0].idTipoUsuario = resultado.idTipoUsuario;

			res.json(resultado[0]);

		} else {
			res.json({
				estado: false,
				mensaje: "No hay usuarios"
			});
		}
	});
});
router.get('/auths/', function (req, res) {
	usuario.token(function(resultado){
		if(typeof resultado !== undefined){
			var doc = new pdf;
			
			doc.pipe(fs.createWriteStream('tokens.pdf'));
			res.setHeader('Content-type', 'application/pdf')
			
			let texto = JSON.stringify(resultado);
			doc.text('TOKENS: ' +texto, 100, 100);
			doc.pipe(res);
			
			doc.end();
			/*doc.output(function(pdf){
				res.type('application/pdf');
				res.end(pdf, 'binary');
			})*/

			//res.json(resultado);


		} else {
			res.json({"mensaje" : "no token"});
		}
	});
});
router.get('/auth/:id', function(req, res){
	var id = req.params.id;
	usuario.selectEs(id, function(resultado){
		if(typeof resultado !== undefined) {
      res.json(resultado[0]);
    } else {
      res.json({"mensaje" : "No hay usuarios"});
    }
	});
})

module.exports = router;