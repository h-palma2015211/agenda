var express = require('express');
var materia = require('../../model/materia.model');
var router = express.Router();
var randToken = require('rand-token').suid;

router.get('/materia/', function(req, res, next) {
    materia.select(function(materias){
        if(typeof materias !== 'undefined'){
            res.json(materias);
        } else {
            res.json({'mensaje': 'no hay materias'});
        }
    });
});

router.get('/materia/:idMateria', function(req, res, next){
    idMateria = req.params.idMateria;
    materia.selectMateria(idMateria, function(materias){
         if(typeof materias !== 'undefined'){
            res.json(materias.find(c => c.idMateria == idMateria))
        } else {
            res.json({'mensaje': 'no hay materias'});
        }
    })
});

router.get('/materias/:idUsuario', function(req, res, next){
    idUsuario = req.params.idUsuario;
    materia.asignada(idUsuario, function(mate){
        if(typeof mate !== 'undefined'){
            res.json(mate);
        } else {
            res.json({'mensaje': 'no hay mate'});
        }
    })
});

router.post('/materia', function(req, res, next){
   var token = randToken(16);
    var data = {
        nombre: req.body.nombre,
        token: token    
    }
    materia.insert(data, function(resultado){
        if(resultado && resultado.affectedRows > 0) {
      res.json(resultado);  
    } else {
      res.json({"mensaje":"No se ingreso el materia"});
    }
    });
});

router.put('/materia/:idMateria', function(req, res, next){
 var idMateria = req.params.idMateria;
    var data = {
        nombre: req.body.nombre,
        idMateria: idMateria
    }

    if(idMateria == data.idMateria) {
        materia.update(data, function(resultado){
             if(typeof resultado !== undefined) {
                res.json(resultado);
              } else {
                console.log("NO: " + resultado.length);
        //res.json({"estatus": "false"});
                res.end();
              }
        });
    }
});

router.delete('/materia/:idMateria', function(req, res, next){
    var id = req.params.idMateria;
    materia.delete(id, function(resultado){
        if(resultado && resultado.mensaje ===	"Eliminado") {
        res.json({"mensaje":"Se elimino la materia correctamente"});
      } else {
        res.json({"mensaje":"Se elimino la materia"});
      }
    });
});

module.exports = router;