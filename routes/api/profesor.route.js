var express = require('express');
var profesor = require('../../model/profesor.model');
var router = express.Router();

router.get('/profesor/', function(req, res, next){
    profesor.selectAll(function(profesores){
        if(typeof profesores !== 'undefined'){
            res.json(profesores);
        } else {
            res.json({'mensaje': 'no hay tareas'});
        }
    })
});
router.get('/asignar/', function(req, res, next) {
    profesor.mostrarAsi(function(asignaciones){
        if(typeof asignaciones !== 'undefined'){
            res.json(asignaciones);
        } else {
          res.json({'mensaje': 'no hay tareas'});      
        }
    });
});
router.post('/asignar', function(req, res, next){
    data = {
        idMateria: req.body.idMateria,
        idProfesor: req.body.idProfesor,
        idSeccion: req.body.idSeccion
    }
    profesor.asignar(data, function(resultado){
    if(resultado && resultado.affectedRows > 0) {
      res.json(resultado);  
    } else {
      res.json({"mensaje":"No se ingreso el profesor"});
    }
    })
});
router.get('/profesor/:idProfesor', function(req, res, next){
    idProfesor = req.params.idProfesor
    profesor.SelectProfesor(idProfesor, function(profesores){
        if(typeof profesores !== 'undefined'){
            res.json(profesores.find(c => c.idProfesor == idProfesor))
        } else {
            res.json({'mensaje': 'no hay tareas'});
        }
    })
})

router.post('/profesor', function(req, res, next){
    var data = {
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        direccion: req.body.direccion,
        telefono: req.body.telefono,
        nick: req.body.nick,
        contrasena: req.body.contrasena
    }
    profesor.insert(data, function(resultado){
         if(resultado && resultado.affectedRows > 0) {
      res.json(resultado);  
    } else {
      res.json({"mensaje":"No se ingreso el profesor"});
    }
    });
});

router.put('/profesor/:idProfesor', function(req, res, next){
    var idProfesor = req.params.idProfesor;
    var data = {
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        direccion: req.body.direccion,
        telefono: req.body.telefono,
        idProfesor: idProfesor 

    }
    if(idProfesor == data.idProfesor) {
        profesor.update(data, function(resultado){
            if(typeof resultado !== undefined) {
                res.json(resultado);
              } else {
                console.log("NO: " + resultado.length);
        //res.json({"estatus": "false"});
                res.end();
              }
        });
    }
});

router.delete('/profesor/:idProfesor', function(req, res, next){
    var id = req.params.idProfesor;
    profesor.delete(id, function(resultado){
        if(resultado && resultado.mensaje ===	"Eliminado") {
        res.json({"mensaje":"Se elimino la profesor correctamente"});
      } else {
        res.json({"mensaje":"Se elimino la profesor"});
      }
    });
});

module.exports = router;