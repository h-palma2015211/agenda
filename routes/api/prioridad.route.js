var express = require('express');
var prioridad = require('../../model/prioridad.model');
var router = express.Router();

router.get('/prioridad', function(req, res, next) {

  prioridad.select( function(prioridad) {
    if(typeof prioridad !== 'undefined') {
      res.json(prioridad);
    } else {
      res.json({"mensaje" : "No hay prioridades"});
    }
  });
});
module.exports = router;
