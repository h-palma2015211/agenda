var express = require('express');
var tarea = require('../../model/tarea.model');
var router = express.Router();

router.get('/tarea/:id', function(req, res, next) {
  var idTarea = req.params.id;
  tarea.select(idTarea, function(tareas) {
    if(typeof tareas !== 'undefined') {
      res.json(tareas);
    } else {
      res.json({"mensaje" : "No hay tareas"});
    }
  });
});


router.post('/tarea', function(req, res, next) {
    var data = {
        nombre: req.body.nombre,
        descripcion: req.body.descripcion,
        fecha: req.body.fecha,
        categoria: req.body.categoria,
        idPrioridad: req.body.idPrioridad,
        idUsuario: req.body.idUsuario
    };
    tarea.insert(data, function(resultado){
        if(resultado && resultado.affectedRows > 0) {
      res.json(resultado);  
    } else {
      res.json({"mensaje":"No se ingreso el tarea"});
    }
    });
});

router.put('/tarea/:id', function(req, res, next) {
    var idTarea = req.params.idTarea;
    var data = {
        nombre: req.body.nombre,
        descripcion: req.body.descripcion,
        fecha: req.body.fecha,
        categoria: req.body.categoria,
        idPrioridad: req.body.idPrioridad,
        idTarea: idTarea
    };
    if(idTarea == data.idTarea) {
        tarea.update(data, function(resultado){
             if(typeof resultado !== undefined) {
                res.json(resultado);
              } else {
                console.log("NO: " + resultado.length);
        //res.json({"estatus": "false"});
                res.end();
            }
        });
    } else {
    res.json({"mensaje": "No coinciden los identificadores"});
    }
});

router.delete('/tarea/:idTarea', function(req, res, next){
    var id = req.params.idTarea;
    tarea.delete(id, function(resultado){
        if(resultado && resultado.mensaje ===	"Eliminado") {
        res.json({"mensaje":"Se elimino la contacto correctamente"});
      } else {
        res.json({"mensaje":"Se elimino la contacto"});
      }
    });

});

module.exports = router;


