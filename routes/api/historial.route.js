var express = require('express');
var historial = require('../../model/historial.model');
var services = require('../../services');
var router = express.Router();


router.get('/historial', function(req, res){
    historial.select(function(resultados){
        if(typeof resultados !== undefined) {
      res.json(resultados);
    } else {
      res.json({"mensaje" : "No hay historiales"});
    }
    });
});
module.exports = router;
