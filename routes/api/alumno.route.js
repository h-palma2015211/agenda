var express = require('express');
var alumno = require('../../model/alumno.model');
var router = express.Router();

router.get('/alumno/:idUsuario', function(req, res, next){
    idUsuario = req.params.idUsuario;
    alumno.select(idUsuario, function(resultados){
        if(typeof resultados !== 'undefined') {
            res.json(resultados[0]);
        } else {
            res.json({'mensaje': 'no hay asignaciones'});
        }
    })
});
module.exports = router;