var express = require('express');
var seccion = require('../../model/seccion.model');
var router = express.Router();

router.get('/seccion/', function(req, res, next){
    seccion.select(function(seccion){
        if(typeof seccion !== 'undefined'){
            res.json(seccion);
        } else {
            res.json({'Mensaje': 'no hay secciones'})
        }
    })
})

module.exports = router;